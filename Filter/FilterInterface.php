<?php

namespace NM\Bundle\WebsiteBundle\Filter;

use eZ\Publish\API\Repository\Values\Content\Query\Criterion\LogicalOperator;
use Pagerfanta\Pagerfanta;

/**
 * Interface FilterInterface.
 */
interface FilterInterface
{
    /**
     * @param Pagerfanta $currentResult
     *
     * @return array
     */
    public function getAll(Pagerfanta $currentResult);

    /**
     * @param string
     *
     * @return null|LogicalOperator
     */
    public function getCriterion($getParameter);

    /**
     * @param bool $plural
     *
     * @return string
     */
    public function getName($plural = false);
}
