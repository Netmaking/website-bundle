<?php

namespace NM\Bundle\WebsiteBundle\Filter;

use Symfony\Component\Routing\Exception\ResourceNotFoundException;

/**
 * Class FilterChain.
 */
class FilterChain
{
    /**
     * @var array Collection of ViewmodeInterface
     */
    protected $filters = [];

    /**
     * @param FilterInterface $filter
     */
    public function add(FilterInterface $filter)
    {
        $this->filters[$filter->getName()] = $filter;
    }

    /**
     * @param string $identifier
     *
     * @return FilterInterface
     *
     * @throws ResourceNotFoundException
     */
    public function get($identifier)
    {
        if (!$this->exist($identifier)) {
            throw new ResourceNotFoundException(
                sprintf('Could not find a filter named %s.', $identifier)
            );
        }

        return $this->filters[$identifier];
    }

    /**
     * @param string $identifier
     *
     * @return bool
     */
    public function exist($identifier)
    {
        return array_key_exists($identifier, $this->filters);
    }
}
