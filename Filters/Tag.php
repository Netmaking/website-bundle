<?php

namespace NM\Bundle\WebsiteBundle\Filters;

use NM\Bundle\WebsiteBundle\Service\NMTagService;
use NM\Bundle\WebsiteBundle\Filter\FilterInterface;
use eZ\Publish\API\Repository\Values\Content\Query\Criterion;
use Pagerfanta\Pagerfanta;

class Tag implements FilterInterface
{
    /** @var NMTagService $nmTag */
    protected $nmTag;

    /**
     * @param NMTagService $nmTag
     */
    public function __construct(NMTagService $nmTag)
    {
        $this->nmTag = $nmTag;
    }

    /**
     * @inheritdoc
     */
    public function getAll(Pagerfanta $currentResult)
    {
        /* Get all tags in current subtree */
        $currentResult->setMaxPerPage(PHP_INT_MAX);
        $results = $currentResult->getCurrentPageResults();

        return $this->nmTag->getTagsInSubtree($results);
    }

    /**
     * @inheritdoc
     */
    public function getCriterion($currentValue)
    {
        /*
         * @todo Implementation of eZ\Publish\SPI\FieldType\Indexable in NetgenTagsBundle
         * @see https://github.com/netgen/TagsBundle/issues/25
         */
        $tagNewStacksearchIsImplemented = false;
        if ($tagNewStacksearchIsImplemented) {
            return new Criterion\LogicalAnd(
                    new Criterion\Field('tags', Criterion\Operator::CONTAINS, $currentValue)
            );
        }

        return;
    }

    /**
     * @inheritdoc
     */
    public function getName($plural = false)
    {
        return $plural ? 'tags' : 'tag';
    }
}
