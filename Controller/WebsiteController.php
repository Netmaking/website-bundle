<?php

namespace NM\Bundle\WebsiteBundle\Controller;

use eZ\Bundle\EzPublishCoreBundle\Controller;
use eZ\Publish\API\Repository\ContentTypeService;
use eZ\Publish\API\Repository\Exceptions\PropertyNotFoundException;
use eZ\Publish\API\Repository\Values\Content\Content;
use eZ\Publish\Core\MVC\Symfony\View\Provider\Location;
use Netgen\TagsBundle\API\Repository\Values\Content\Query\Criterion\TagKeyword;
use Netgen\TagsBundle\Core\SignalSlot\TagsService;
use NM\Bundle\WebsiteBundle\Service\NMMenuService;
use Symfony\Component\HttpFoundation\Response;
use NM\Bundle\WebsiteBundle\Service\NMContentService;
use NM\Bundle\WebsiteBundle\Service\NMFieldService;
use NM\Bundle\WebsiteBundle\Service\NMLocationService;
use NM\Bundle\WebsiteBundle\Service\NMSearchService;
use eZ\Publish\Core\Base\Exceptions\UnauthorizedException;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use WhiteOctober\BreadcrumbsBundle\Templating\Helper\BreadcrumbsHelper;
use eZ\Publish\Core\Helper\TranslationHelper;
use Tedivm\StashBundle\Service\CacheService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Cache;

class WebsiteController extends Controller
{
    /**
     * @param int    $locationId
     * @param string $viewType
     * @param bool   $layout
     * @param array  $params
     *
     * @throws AccessDeniedException
     * @throws UnauthorizedException
     * @throws \Exception
     *
     * @return Response
     */
    public function fullAction($locationId, $viewType, $layout = false, array $params = array())
    {
        /** @var NMLocationService $nmLocation */
        $nmLocation = $this->get('nm.location');
        /** @var NMFieldService $nmField */
        $nmField = $this->get('nm.field');

        $rootLocationId = $this->getConfigResolver()->getParameter('content.tree_root.location_id');
        $location = $nmLocation->getEzLocationService()->loadLocation($locationId);

        $params['frontpage'] = $nmField->getFieldRelation($rootLocationId, 'frontpage');
        $params['children'] = $nmLocation->getEzLocationService()->loadLocationChildren($location);

        $params['google_analytics_id'] = $this->container->getParameter('nm.googleanalytics.id');

        return $nmLocation->generateResponse($locationId, $viewType, $layout, $params);
    }

    protected function mapContentByTypeName($contentObjects)
    {
        $result = array();

        $contentTypeHasRelatedFields = array(
                'article_list' => array(
                    'article'
                )
        );

        $repository = $this->container->get('ezpublish.api.repository');

        /** @var ContentTypeService $contentTypeService */
        $contentTypeService = $repository->getContentTypeService();
        $result['related_objects'] = array();

        /** @var Location $location */
        foreach ($contentObjects as $content)
        {

            $contentTypeIdentifier = $contentTypeService->loadContentType($content->versionInfo->contentInfo->contentTypeId)->identifier;

            if (array_key_exists($contentTypeIdentifier, $result))
            {
                if (is_array($result[$contentTypeIdentifier]))
                {
                    $result[$contentTypeIdentifier][] = $content;
                }
                else
                {
                    $result[$contentTypeIdentifier] = array(
                            $result[$contentTypeIdentifier],
                            $content
                    );
                }
            }
            else
            {
                $result[$contentTypeIdentifier] = $content;
            }

            // TODO: EXTRACT
            if (array_key_exists($contentTypeIdentifier, $contentTypeHasRelatedFields)) {
                foreach ($contentTypeHasRelatedFields[$contentTypeIdentifier] as $field) {
                    try {
                        $destinationContentId = $this->getTranslatedFieldValue($content, $field)->destinationContentId;
                        $result['related_objects'][$destinationContentId] = $this->getRepository()->getContentService()->loadContent($destinationContentId);
                    } catch (PropertyNotFoundException $e) {
                        $destinationContentIds = $this->getTranslatedFieldValue($content, $field)->destinationContentIds;
                        foreach ($destinationContentIds as $destinationContentId) {
                            $result['related_objects'][$destinationContentId] = $this->getRepository()->getContentService()->loadContent($destinationContentId);
                        }
                    }
                }
            }
        }

        return $result;
    }
    // TODO: EXTRACT
    protected function getFooter()
    {
        $result = array();

        /** @var NMLocationService $nmLocation */
        $nmLocation = $this->get('nm.location');

        $nmContent = $this->get('nm.content');

        $rootLocationId = $this->getConfigResolver()->getParameter('content.tree_root.location_id');

        /** @var Location $location */
        $location = $nmLocation->getEzLocationService()->loadLocation($rootLocationId);
        $rootContentObject = $nmContent->getEzContentService()->loadContentByContentInfo($location->contentInfo);

        $footerId = $rootContentObject->getFieldValue("footer")->destinationContentId;
        $footerContentObject = $nmContent->getEzContentService()->loadContent($footerId);
        $footerLocation = $nmLocation->getEzLocationService()->loadLocation($footerContentObject->contentInfo->mainLocationId);
        $footerChildrenLocations = $nmLocation->getEzLocationService()->loadLocationChildren($footerLocation);
        $result["object"] = $footerContentObject;
        $result["children"] = array();
        foreach ($footerChildrenLocations->locations as $child)
        {
            $result["children"][$child->contentInfo->id] = $nmContent
                    ->getEzContentService()
                    ->loadContentByContentInfo(
                            $child->contentInfo
                    );
        }
        return $result;
    }

    public function fullContentAction($locationId, $viewType, $layout = false, array $params = array())
    {
        /** @var NMLocationService $nmLocation */
        $nmLocation = $this->get('nm.location');
        /** @var NMContentService $nmContent */
        $nmContent = $this->get('nm.content');
        /** @var TagsService $tagsService */
        $tagsService = $this->get('ezpublish.api.service.tags');

        $location = $nmLocation->getEzLocationService()->loadLocation($locationId);

        $menuService = $this->get('nm.menu');

        $params["mainmenu"] = $menuService->getMenuItems($locationId);

        $params['children'] = array();
        $childrenLocations = $nmLocation->getEzLocationService()->loadLocationChildren($location);
        foreach ($childrenLocations->locations as $childLocation) {
                $params['children'][] = $nmContent->getEzContentService()->loadContentByContentInfo($childLocation->contentInfo);
        }
        $params["tags"] = $this->getTagsFromContentElements($params["children"]);

        return $nmLocation->generateResponse($locationId, $viewType, $layout, $params);
    }

    private function getTagsFromContentElements($contentElements)
    {
        $result = array();
        /** @var Content $content */
        foreach ($contentElements as $content) {
            if ($tags = $content->getFieldValue("tags")) {
                $result = $result + $content->getFieldValue("tags")->tags;
            }
        }
        return $result;
    }

    /**
     * @Cache(smaxage="3600", maxage="300")
     *
     * @return Response
     */
    public function footerAction()
    {
        /** @var NMFieldService $nmField */
        $nmField = $this->get('nm.field');
        /** @var NMLocationService $nmLocation */
        $nmLocation = $this->get('nm.location');

        $rootLocationId = $this->getConfigResolver()->getParameter('content.tree_root.location_id');
        $footerLocation = $nmField->getFieldRelation($rootLocationId, 'footer');
        $params = array();
        $params['children'] = $nmLocation->getEzLocationService()->loadLocationChildren($footerLocation);

        return $this->container->get('ez_content')->viewLocation(
                $footerLocation->id, 'full', false, $params
        );
    }

    /**
     * @param bool $currentLocationId
     *
     * @return Response
     */
    public function menuAction($currentLocationId = false)
    {
        /** @var NMMenuService $nmMenu */
        $nmMenu = $this->get('nm.menu');
        $mainmenu = $nmMenu->getMenuItems();

        return $this->render('NMWebsiteBundle:Menu:main.html.twig', array(
            'mainmenu' => $mainmenu,
            'currentLocationId' => $currentLocationId,
        ));
    }
}
