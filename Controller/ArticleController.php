<?php

namespace NM\Bundle\WebsiteBundle\Controller;

use eZ\Bundle\EzPublishCoreBundle\Controller;
use eZ\Publish\API\Repository\ContentTypeService;
use eZ\Publish\API\Repository\Values\Content\Content;
use eZ\Publish\API\Repository\Values\Content\Location;
use eZ\Publish\API\Repository\Values\Content\Search\SearchHit;
use NM\Bundle\WebsiteBundle\Service\NMContentService;
use NM\Bundle\WebsiteBundle\Service\NMLocationService;

class ArticleController extends WebsiteController
{
    /**
     * @param int    $locationId
     * @param string $viewType
     * @param bool   $layout
     * @param array  $params
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function fullAction($locationId, $viewType, $layout = false, array $params = array())
    {
        /** @var NMLocationService $nmLocation */
        $nmLocation = $this->get('nm.location');
        /** @var NMContentService $nmContent */
        $nmContent = $this->get('nm.content');

        $location = $nmLocation->getEzLocationService()->loadLocation($locationId);

        $params["article"] = $nmContent->getEzContentService()->loadContentByContentInfo($location->contentInfo);
        $menuService = $this->get('nm.menu');
        $params["mainmenu"] = $menuService->getMenuItems($locationId);
        $params["footer"] = $this->getFooter();
        $params["files"] = $this->getFiles($location);
        $params["breadcrumbs"] = $this->getBreadCrumbs($locationId);

        $params["related_objects"] = $this->getRelatedElements($nmContent->getEzContentService()->loadContentByContentInfo($location->contentInfo));


        return $nmLocation->generateResponse($locationId, $viewType, $layout, $params);
    }

    public function getRelatedElements(Content $content)
    {
        $nmContent = $this->get('nm.content');
        $ezContent = $nmContent->getEzContentService();
        $relations = $ezContent->loadRelations( $content->versionInfo );
        $repository = $this->container->get('ezpublish.api.repository');
        /** @var ContentTypeService $contentTypeService */
        $contentTypeService = $repository->getContentTypeService();

        $result = array();
        foreach ($relations as $relation)
        {
            if($relation->type == 8 || $relation->type == 1)
            {
                $content = $ezContent->loadContent($relation->destinationContentInfo->id);
                $id = $relation->destinationContentInfo->id;
                $versionInfo = $content->versionInfo;
                $contentTypeIdentifier = $contentTypeService->loadContentType($versionInfo->contentInfo->contentTypeId)->identifier;
                $result[$contentTypeIdentifier][$id] = $content;
            }

        }

        return $result;
    }

    public function getFiles($location)
    {
        $result = array();
        /** @var NMLocationService $nmLocation */
        $nmLocation = $this->get('nm.location');
        /** @var NMContentService $nmContent */
        $nmContent = $this->get('nm.content');

        $files = $nmLocation->getLocationsByIdentifier($location->id, array('endowment_file'));

        /** @var SearchHit $file */
        foreach ($files->searchHits as $file)
        {
            $result[] = $nmContent
                            ->getEzContentService()
                            ->loadContentByContentInfo($file->valueObject->contentInfo);
        }
        return $result;
    }

    public function getBreadCrumbs($locationId)
    {
        /** @var BreadcrumbsHelper $breadcrumbs */
        $breadcrumbs = array();

        $locationService = $this->getRepository()->getLocationService();
        $path = $locationService->loadLocation($locationId)->path;

        // The root location can be defined at site access level
        $rootLocationId = $this->getConfigResolver()->getParameter('content.tree_root.location_id');

        /** @var TranslationHelper $translationHelper */
        $translationHelper = $this->get('ezpublish.translation_helper');

        $isRootLocation = false;

        // Shift of location "1" from path as it is not a fully valid location and not readable by most users
        array_shift($path);

        for ($i = 0; $i < count($path); $i++) {
            $location = $locationService->loadLocation($path[$i]);
            // if root location hasn't been found yet
            if (!$isRootLocation) {
                // If we reach the root location We begin to add item to the breadcrumb from it
                if ($location->id == $rootLocationId) {
                    $isRootLocation = true;
                    $breadcrumbs[$this->get('translator')->trans('Home')] = $this->generateUrl($location);
                }
            } else { // The root location has already been reached, so we can add items to the breadcrumb
                $breadcrumbs[$translationHelper->getTranslatedContentNameByContentInfo($location->contentInfo)] = $this->generateUrl($location);
            }
        }
        return $breadcrumbs;
    }
}
