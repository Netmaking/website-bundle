<?php

namespace NM\Bundle\WebsiteBundle\Controller;

use eZ\Bundle\EzPublishCoreBundle\Controller;
use eZ\Publish\API\Repository\ContentTypeService;
use eZ\Publish\API\Repository\Values\Content\Content;
use eZ\Publish\API\Repository\Values\Content\Location;
use NM\Bundle\WebsiteBundle\Service\NMContentService;
use NM\Bundle\WebsiteBundle\Service\NMLocationService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Cache;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

class FrontpageController extends WebsiteController
{
    /**
     * Main action for viewing content through a location in the repository.
     *
     * @Cache(smaxage="3600", maxage="300")
     *
     * @param int $locationId
     * @param string $viewType
     * @param bool $layout
     * @param array $params
     *
     * @throws AccessDeniedException
     * @throws \Exception
     *
     * @return Response
     */
    public function fullAction($locationId, $viewType, $layout = false, array $params = array())
    {
        /** @var NMLocationService $nmLocation */
        $nmLocation = $this->get('nm.location');
        /** @var Location $location */
        $location = $nmLocation->getEzLocationService()->loadLocation($locationId);
        /** @var NMContentService $contentService */
        $nmContentService = $this->get('nm.content');

        $menuService = $this->get('nm.menu');
        $children = $nmLocation->getChildrenOfRelationField($locationId, "front_page");

        $params["mainmenu"] = $menuService->getMenuItems($locationId);
        $params["footer"] = $this->getFooterObjects();

        $childrenAsContent = array();
        foreach ($children->locations as $childLocation)
        {
            $contentInfo = $childLocation->contentInfo;
            $childrenAsContent[] = $nmContentService->getEzContentService()->loadContent($contentInfo->id);
        }

        $mappedChildren = $this->mapContentByTypeName($childrenAsContent);

        return $nmLocation->generateResponse($locationId, $viewType, $layout, array_merge($params, $mappedChildren));
    }

    public function getTranslatedFieldValue(Content $content, $fieldDefIdentifier, $forcedLanguage = null)
    {
        $translationHelper = $this->container->get('ezpublish.translation_helper');
        return $translationHelper->getTranslatedField($content, $fieldDefIdentifier, $forcedLanguage)->value;
    }

    private function getFooterObjects()
    {
        $result = array();

        /** @var NMLocationService $nmLocation */
        $nmLocation = $this->get('nm.location');

        $nmContent = $this->get('nm.content');

        $rootLocationId = $this->getConfigResolver()->getParameter('content.tree_root.location_id');

        /** @var Location $location */
        $location = $nmLocation->getEzLocationService()->loadLocation($rootLocationId);
        $rootContentObject = $nmContent->getEzContentService()->loadContentByContentInfo($location->contentInfo);

        $footerId = $rootContentObject->getFieldValue("footer")->destinationContentId;
        $footerContentObject = $nmContent->getEzContentService()->loadContent($footerId);
        $footerLocation = $nmLocation->getEzLocationService()->loadLocation($footerContentObject->contentInfo->mainLocationId);
        $footerChildrenLocations = $nmLocation->getEzLocationService()->loadLocationChildren($footerLocation);
        $result["object"] = $footerContentObject;
        $result["children"] = array();
        foreach ($footerChildrenLocations->locations as $child)
        {
            $result["children"][$child->contentInfo->id] = $nmContent
                    ->getEzContentService()
                    ->loadContentByContentInfo(
                            $child->contentInfo
                    );
        }
        return $result;

    }

}
