<?php

namespace NM\Bundle\WebsiteBundle\Controller;

use eZ\Bundle\EzPublishCoreBundle\Controller;
use eZ\Publish\API\Repository\Values\Content\Query\Criterion\Operator;
use eZ\Publish\API\Repository\Values\Content\Content;
use eZ\Publish\API\Repository\Values\Content\Location;
use Netgen\TagsBundle\API\Repository\Values\Content\Query\Criterion\TagKeyword;
use NM\Bundle\WebsiteBundle\Pagination\LocationSearchAdapter;
use NM\Bundle\WebsiteBundle\Service\NMLocationService;
use NM\Bundle\WebsiteBundle\Service\NMContentService;
use NM\Bundle\WebsiteBundle\Service\NMLegacyService;
use NM\Bundle\WebsiteBundle\Service\NMSearchService;
use NM\Bundle\WebsiteBundle\Viewmode\Viewmode;
use Pagerfanta\Pagerfanta;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
#use Netgen\Bundle\EnhancedSelectionBundle\Core\FieldType\EnhancedSelection\Value as EnhancedSelectionValue;
use eZ\Publish\API\Repository\Values\Content\LocationQuery;
use NM\Bundle\WebsiteBundle\Filter\FilterInterface;
use Tedivm\StashBundle\Service\CacheService;

class FolderController extends WebsiteController
{
    /**
     * @param Request $request
     * @param int     $locationId
     * @param string  $viewType
     * @param bool    $layout
     * @param array   $params
     *
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @throws \Exception
     * @throws \InvalidArgumentException
     */
    public function renderByDisplayOptionAction(Request $request, $locationId, $viewType, $layout = false, array $params = array())
    {
        $pool = $this->container->get('ezpublish.cache_pool');

        /** @var NMLocationService $nmLocation */
        $nmLocation = $this->get('nm.location');
        /** @var NMContentService $nmContent */
        $nmContent = $this->get('nm.content');
        /** @var NMSearchService $nmSearch */
        $nmSearch = $this->get('nm.search');

        $location = $nmLocation->getEzLocationService()->loadLocation($locationId);
        $content = $nmContent->getEzContentService()->loadContent($location->contentId);

        $viewmode = $this->get('nm.folder.viewmode.chain')->get(
                $this->getViewmode($content)
        );

        $viewModeParams = $this->handleViewmode($request, $viewmode, $locationId);

        $params = array_merge($params, $viewModeParams);
        $menuService = $this->get('nm.menu');
        $params["mainmenu"] = $menuService->getMenuItems($locationId);
        $params["footer"] = $this->getFooter();
        $params['related_objects'] = $this->getRelatedElements($nmContent->getEzContentService()->loadContentByContentInfo($location->contentInfo));

        /** @var Response $response */
        $response = $nmLocation->generateResponse($locationId, $viewType, $layout, $params);


        return $this->render(
                $viewmode->getTemplate(),
                $params + array(
                        'location' => $location,
                        'content' => $content,
                ),
                $response
        );
    }

    public function renderContentByDisplayOptionAction(Request $request, $contentId, $viewType, $layout = false, array $params = array())
    {
        $pool = $this->container->get('ezpublish.cache_pool');

        /** @var NMLocationService $nmLocation */
        $nmLocation = $this->get('nm.location');
        /** @var NMContentService $nmContent */
        $nmContent = $this->get('nm.content');
        /** @var NMSearchService $nmSearch */
        $nmSearch = $this->get('nm.search');


        $content = $nmContent->getEzContentService()->loadContent($contentId);
        $location = $nmLocation->getEzLocationService()->loadLocation($content->contentInfo->mainLocationId);


        $viewmode = $this->get('nm.folder.viewmode.chain')->get(
                $this->getViewmode($content)
        );

        $viewModeParams = $this->handleViewmode($request, $viewmode, $content->contentInfo->mainLocationId);

        $params = array_merge($params, $viewModeParams);

        /** @var Response $response */
        $response = $nmLocation->generateResponse($content->contentInfo->mainLocationId, $viewType, $layout, $params);

        $menuService = $this->get('nm.menu');
        $params["mainmenu"] = $menuService->getMenuItems($content->contentInfo->mainLocationId);
        $params["footer"] = $this->getFooter();
        return $this->render(
                $viewmode->getTemplate(),
                $params + array(
                        'location' => $location,
                        'content' => $content,
                ),
                $response
        );
    }

    /**
     * @param \eZ\Publish\API\Repository\Values\Content\Content $content
     *
     * @return string
     */
    protected function getViewmode($content)
    {

        /** @var EnhancedSelectionValue $viewmode */
        $viewmode = $content->getFieldValue('viewmode');

        if (isset($viewmode->identifiers) && isset($viewmode->identifiers[0])) {
            return $viewmode->identifiers[0];
        } else  if(isset($viewmode->selection)) {
            switch ($viewmode->selection[0]) {
                case '0':
                    return "image_gallery";
                    break;
                default:
                    return "endowment_article";
                    break;
            }
        }
        return 'endowment_article';
    }

    protected function handleViewmode($request, Viewmode $viewmode, $locationId)
    {
        /** @var NMContentService $nmContent */
        $nmContent = $this->get('nm.content');
        /** @var NMLocationService $nmLocation */
        $nmLocation = $this->get('nm.location');
        /** @var NMSearchService $nmSearch */
        $nmSearch = $this->get('nm.search');

        $params = array();

        $standardCriterions = array();
        if ($currentTag = $request->get('tag', false))
        {
            $viewmode->setCurrentTag($currentTag);
        }

        if (!$sortClause = $viewmode->getSortClause())
        {
            $location = $nmLocation->getEzLocationService()->loadLocation($locationId);
            $nmSortLocation = $this->get('nm.sort_location');
            $sortClause = array($nmSortLocation->getSortClauseFromLocation($location));

        }

        /** @var LocationQuery $locationQuery */
        $locationQuery = $nmSearch->getChildLocationsQuery(
                $locationId,
                $viewmode->getContentTypeIdentifiers(),
                null,
                $viewmode->getFieldCriterions($request),
                $sortClause
        );


        $children = new Pagerfanta(
                new LocationSearchAdapter($locationQuery, $nmSearch->getEzSearchService())
        );

        /**
         * Legacysupport for tags, remove when implemented by NetgenTagsBundle
         * @see https://github.com/netgen/TagsBundle/issues/25
         *
         * @todo Remove legacy dependency
         **/
        if ($currentTag = $request->get('tag', false)) {
            /** @var NMLegacyService $nmLegacy */
            $nmLegacy = $this->get('nm.legacy');
            $params['hideContentIds'] = [];
            $params['current_tag'] = $currentTag;
            $children->setMaxPerPage(PHP_INT_MAX);

            foreach ($children as $child) {
                $content = $nmContent->getEzContentService()->loadContentByContentInfo($child->contentInfo);

                if (!$nmLegacy->nodeHasKeyword($content, $currentTag) || !$currentTag == 'All') {
                    $params['hideContentIds'][] = $child->id;
                }
            }
        } else {
            $params['current_tag'] = $currentTag;
        }

        foreach ($viewmode->getFilters() as $filter) {
            /* @var FilterInterface $filter */
            $params[$filter->getName(true)] = $filter->getAll($children);
        }



        if ($viewmode->usePagination()) {
            /* Configure pager */
            $currentPage = $request->get('page', 1);
            $children->setMaxPerPage($this->container->getParameter('nm.folder.children.limit'));
            $children->setCurrentPage($currentPage);
            $params['currentPage'] = $currentPage;
        }

        $childrenAsContent = array();
        foreach ($children->getCurrentPageResults() as $child) {
            $childrenAsContent[] = $nmContent->getEzContentService()->loadContentByContentInfo($child->contentInfo);
        }

        $params["children"] = $children;
        $params["children_content"] = $childrenAsContent;
        return $params;
    }

    public function getRelatedElements(Content $content)
    {
        $nmContent = $this->get('nm.content');
        $ezContent = $nmContent->getEzContentService();
        $relations = $ezContent->loadRelations( $content->versionInfo );
        $repository = $this->container->get('ezpublish.api.repository');
        /** @var ContentTypeService $contentTypeService */
        $contentTypeService = $repository->getContentTypeService();

        $result = array();
        foreach ($relations as $relation)
        {
            if($relation->type == 8 || $relation->type == 1)
            {
                $content = $ezContent->loadContent($relation->destinationContentInfo->id);
                $id = $relation->destinationContentInfo->id;
                $versionInfo = $content->versionInfo;
                $contentTypeIdentifier = $contentTypeService->loadContentType($versionInfo->contentInfo->contentTypeId)->identifier;
                $result[$contentTypeIdentifier][$id] = $content;
            }

        }

        return $result;
    }
}
