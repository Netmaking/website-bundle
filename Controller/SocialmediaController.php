<?php

namespace NM\Bundle\WebsiteBundle\Controller;

use eZ\Bundle\EzPublishCoreBundle\Controller;
use Symfony\Component\HttpFoundation\Response;
use NM\WebsiteBundle\Service\NMLocationService;
use NM\WebsiteBundle\Service\NMFieldService;
use Abraham\TwitterOAuth\TwitterOAuth;
use Tedivm\StashBundle\Service\CacheService;

class SocialmediaController extends Controller
{
    /**
     * Main action for viewing content through a location in the repository.
     *
     * @param int    $locationId
     * @param string $viewType
     * @param bool   $layout
     * @param array  $params
     *
     * @return Response
     */
    public function footerAction($locationId, $viewType, $layout = false, array $params = array())
    {
        /** @var NMLocationService $nmLocation */
        $nmLocation = $this->get('nm.location');

        /** @var CacheService $pool */
        $pool = $this->container->get('ezpublish.cache_pool');
        $cacheKeys = ['footer', 'twitter', 'static'];
        $cache = $pool->getItem($cacheKeys);

        $params = $cache->get();

        if ($cache->isMiss()) {
            /** @var NMFieldService $nmField */
            $nmField = $this->get('nm.field');

            /** @var \eZ\Publish\API\Repository\Values\ValueObject $facebookValueObject */
            $facebookValueObject = $nmField->getFieldValue($locationId, 'show_facebook');
            if (isset($facebookValueObject->bool) && $facebookValueObject->bool === true) {
                $params['facebook'] = $this->getFacebookLikes();
            }

            /** @var \eZ\Publish\API\Repository\Values\ValueObject $twitterValueObject */
            $twitterValueObject = $nmField->getFieldValue($locationId, 'show_twitter');
            if (isset($twitterValueObject->bool) && $twitterValueObject->bool === true) {
                $params['tweets'] = $this->getTwitterTweets();
            }

            $cache->set(
                $params,
                $this->container->getParameter('nm_website.ttl.footer_twitter')
            );
        }

        return $nmLocation->generateResponse($locationId, $viewType, $layout, $params);
    }

    private function getTwitterTweets()
    {
        $connection = new TwitterOAuth(
            $this->container->getParameter('nm.twitter.consumer_key'),
            $this->container->getParameter('nm.twitter.consumer_secret')
        );
        $apiResponse = $connection->get('statuses/user_timeline', array(
            'screen_name' => $this->container->getParameter('nm.twitter.username'),
            'count' => 1,
        ));

        $tweets = [];
        foreach ($apiResponse as $tweet) {
            $tweets[] = array(
                'text' => $tweet->text,
                'username' => $tweet->user->screen_name,
            );
        }

        return $tweets;
    }

    private function getFacebookLikes()
    {
        $facebook = array();
        $facebook['username'] = $this->container->getParameter('nm.facebook.username');

        $query = "select like_count from link_stat where url='https://www.facebook.com/{$facebook['username']}'";

        $call = 'https://api.facebook.com/method/fql.query?query='.rawurlencode($query).'&format=json';

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $call);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $output = json_decode(curl_exec($ch));
        curl_close($ch);

        if (isset($output[0]) && isset($output[0]->like_count)) {
            $facebook['like_count'] = $output[0]->like_count;
        }

        return $facebook;
    }
}
