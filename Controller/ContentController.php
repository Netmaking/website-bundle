<?php

namespace NM\Bundle\WebsiteBundle\Controller;

use eZ\Bundle\EzPublishCoreBundle\Controller;
use NM\WebsiteBundle\Service\NMLocationService;
use NM\WebsiteBundle\Service\NMContentService;

class ContentController extends Controller
{
    /**
     * @param int    $locationId
     * @param string $viewType
     * @param bool   $layout
     * @param array  $params
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function renderWithChildrenAction($locationId, $viewType, $layout = false, array $params = array())
    {
        /** @var NMLocationService $nmLocation */
        $nmLocation = $this->get('nm.location');
        /** @var NMContentService $nmContent */
        $nmContent = $this->get('nm.content');

        $location = $nmLocation->getEzLocationService()->loadLocation($locationId);
        $childrenLocations = $nmLocation->getEzLocationService()->loadLocationChildren($location);

        $params = ['children' => [
            'totalCount' => 0,
            'contents' => [],
        ]];
        if ($childrenLocations->totalCount > 0) {
            foreach ($childrenLocations->locations as $childrenLocation) {
                $params['children']['contents'][] = $nmContent->getEzContentService()->loadContent($childrenLocation->contentId);
                $params['children']['totalCount']++;
            }
        }

        return $nmLocation->generateResponse($locationId, $viewType, $layout, $params);
    }
}
