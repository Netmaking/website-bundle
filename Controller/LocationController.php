<?php

namespace NM\Bundle\WebsiteBundle\Controller;

use eZ\Bundle\EzPublishCoreBundle\Controller;
use NM\WebsiteBundle\Service\NMLocationService;
use NM\WebsiteBundle\Service\NMContentService;
use NM\WebsiteBundle\Service\NMContentTypeService;
use NM\WebsiteBundle\Service\NMFieldService;
use NM\WebsiteBundle\Service\NMFieldTypeService;
use eZ\Publish\API\Repository\Values\Content\Content;
use Tedivm\StashBundle\Service\CacheService;

class LocationController extends Controller
{
    /**
     * @param int    $locationId
     * @param string $viewType
     * @param bool   $layout
     * @param array  $params
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function renderAction($locationId, $viewType, $layout = false, array $params = array())
    {
        /** @var NMLocationService $nmLocation */
        $nmLocation = $this->get('nm.location');

        if (!array_key_exists('skip_relation_attributes', $params) || true !== $params['skip_relation_attributes']) {
            $params += $this->getContentsOfRelationAttributes($locationId);
        }

        return $nmLocation->generateResponse($locationId, $viewType, $layout, $params);
    }

    /**
     * @param int    $locationId
     * @param string $viewType
     * @param bool   $layout
     * @param array  $params
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function renderWithChildrenAction($locationId, $viewType, $layout = false, array $params = array())
    {
        /** @var NMLocationService $nmLocation */
        $nmLocation = $this->get('nm.location');
        $location = $nmLocation->getEzLocationService()->loadLocation($locationId);
        $params['children'] = $nmLocation->getEzLocationService()->loadLocationChildren($location);

        return $nmLocation->generateResponse($locationId, $viewType, $layout, $params);
    }

    /**
     * Returns false if there are no relation attributes or if all of them are not set/empty.
     * Otherwise returns an array of Contents with the fieldtype attributes as array keys.
     *
     * @param int $locationId
     *
     * @return array|false
     *
     * @example array[
     *      'identifier_of_ezobjectrelation' => [ \Content ],
     *      'identifier_of_ezobjectrelationlist' => [ \Content, \Content, \Content ],
     * ]
     */
    protected function getContentsOfRelationAttributes($locationId)
    {
        /** @var CacheService $pool */
        $pool = $this->container->get('ezpublish.cache_pool');
        $cacheKeys = ['relation_attributes', 'location', $locationId];
        $cache = $pool->getItem($cacheKeys);

        if ($cache->isMiss()) {

            /** @var NMLocationService $nmLocation */
            $nmLocation = $this->get('nm.location');
            /** @var NMContentService $nmContent */
            $nmContent = $this->get('nm.content');
            /** @var NMContentTypeService $nmContentType */
            $nmContentType = $this->get('nm.contenttype');
            /** @var NMFieldService $nmField */
            $nmField = $this->get('nm.field');
            /** @var NMFieldTypeService $nmFieldType */
            $nmFieldType = $this->get('nm.fieldtype');

            $params = [];
            $location = $nmLocation->getEzLocationService()->loadLocation($locationId);
            $content = $nmContent->getEzContentService()->loadContent($location->contentId);
            $contentType = $nmContentType->getEzContentTypeService()->loadContentType($content->contentInfo->contentTypeId);

            foreach ($contentType->fieldDefinitions as $fieldDefinition) {
                $fieldType = $nmFieldType->getEzFieldTypeService()->getFieldType($fieldDefinition->fieldTypeIdentifier);
                $fieldValueObject = $nmField->getFieldValue($locationId, $fieldDefinition->identifier);
                if (false === $fieldValueObject) {
                    continue;
                }

                switch ($fieldType->getFieldTypeIdentifier()) {
                    case 'ezobjectrelation':
                        /** @var Content $content */
                        $content = $nmContent->getEzContentService()->loadContent($fieldValueObject->destinationContentId);
                        $location = $nmLocation->getEzLocationService()->loadLocation($content->contentInfo->mainLocationId);
                        if(!$location->hidden && !$location->invisible) {
                            $params[$fieldDefinition->identifier] = $content;
                        }
                        break;
                    case 'ezobjectrelationlist':
                        $relationListContent = [];
                        foreach ($fieldValueObject->destinationContentIds as $contentId) {
                            $content = $nmContent->getEzContentService()->loadContent($contentId);
                            $location = $nmLocation->getEzLocationService()->loadLocation($content->contentInfo->mainLocationId);
                            if(!$location->hidden && !$location->invisible) {
                                $relationListContent[] = $content;
                            }
                        }
                        $params[$fieldDefinition->identifier] = $relationListContent;
                        break;
                }
            }

            $cache->set(
                $params,
                $this->container->getParameter('nm_website.ttl.relations')
            );

        }else{
            $params = $cache->get();
        }

        return count($params) > 0 ? $params : array();
    }
}
