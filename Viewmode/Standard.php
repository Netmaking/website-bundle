<?php

namespace NM\Bundle\WebsiteBundle\Viewmode;

use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Symfony\Component\HttpFoundation\Request;

class Standard implements ViewmodeInterface
{
    protected $container;

    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    /**
     * @inheritdoc
     */
    public function getFieldCriterions(Request $request)
    {
        return;
    }

    /**
     * @inheritdoc
     */
    public function getSortClause()
    {
        return;
    }

    /**
     * @inheritdoc
     */
    public function getContentTypeIdentifiers()
    {
        return;
    }

    /**
     * @inheritdoc
     */
    public function getTemplate()
    {
        return 'NMWebsiteBundle:Folder:standard.html.twig';
    }

    /**
     * @inheritdoc
     */
    public function getIdentifier()
    {
        return 'line_standard';
    }

    /**
     * @inheritdoc
     */
    public function usePagination()
    {
        return true;
    }

    /**
     * @inheritdoc
     */
    public function getFilters()
    {
        return [];
    }
}
