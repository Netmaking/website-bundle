<?php

namespace NM\Bundle\WebsiteBundle\Viewmode;

use eZ\Publish\API\Repository\Values\Content\Query\Criterion;
use Symfony\Component\HttpFoundation\Request;
use NM\WebsiteBundle\Filter\FilterInterface;

abstract class Viewmode implements ViewmodeInterface
{
    protected $filters;

    public function __construct(array $filters = array())
    {
        $this->filters = $filters;
    }

    /**
     * @param Request $request
     *
     * @return array An array of criterion filters
     */
    protected function getFilterCriterions(Request $request)
    {
        $criterions = [];

        foreach ($this->getFilters() as $filter) {
            /** @var FilterInterface $filter */
            if ($param = $request->get($filter->getName())) {
                $criterion = $filter->getCriterion($param);
                if (null !== $criterion) {
                    $criterions[] = $criterion;
                }
            }
        }

        return $criterions;
    }

    /**
     * @return array
     */
    public function getFilters()
    {
        $filters = [];
        foreach ($this->filters as $filter) {
            /* @var FilterInterface $filter */
            $filters[$filter->getName()] = $filter;
        }

        return $filters;
    }
}
