<?php

namespace NM\Bundle\WebsiteBundle\Viewmode;

use eZ\Publish\API\Repository\Values\Content\Query\Criterion\LogicalAnd;
use Symfony\Component\HttpFoundation\Request;

/**
 * Interface ViewmodeInterface.
 */
interface ViewmodeInterface
{
    /**
     * @param Request $request
     *
     * @return null|LogicalAnd
     */
    public function getFieldCriterions(Request $request);

    /**
     * Return an array with one or more SortClauses, return null to let eZ decide.
     *
     * @return null|array
     */
    public function getSortClause();

    /**
     * Return an array with contenttypeidentifiers in string, or null to accept all contenttypes.
     *
     * @return null|array
     */
    public function getContentTypeIdentifiers();

    /**
     * Return the template you want this view to be rendered in, e.g. NMWebsiteBundle:Folder:standard.html.twig.
     *
     * @return string
     */
    public function getTemplate();

    /**
     * Return the viewmode identifier defined in eZPublish admin.
     *
     * @return string
     */
    public function getIdentifier();

    /**
     * Return whether or not you want pagination to be displayed.
     *
     * @return bool
     */
    public function usePagination();

    /**
     * @return array
     */
    public function getFilters();
}
