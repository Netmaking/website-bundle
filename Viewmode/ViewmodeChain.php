<?php

namespace NM\Bundle\WebsiteBundle\Viewmode;

use Symfony\Component\Routing\Exception\ResourceNotFoundException;

/**
 * Class ViewmodeChain.
 */
class ViewmodeChain
{
    /**
     * @var array Collection of ViewmodeInterface
     */
    protected $viewmodes = [];

    /**
     * @param ViewmodeInterface $viewmode
     */
    public function add(ViewmodeInterface $viewmode)
    {
        $this->viewmodes[$viewmode->getIdentifier()] = $viewmode;
    }

    /**
     * @param string $identifier
     *
     * @return ViewmodeInterface
     *
     * @throws ResourceNotFoundException
     */
    public function get($identifier)
    {
        if (!$this->exist($identifier)) {
            /**
             * @todo Fix bug where "/Ansatte" throws 500
             */
            return $this->viewmodes['line_article'];

            throw new ResourceNotFoundException(
                sprintf('A viewmode for %s is not implemented.', $identifier)
            );
        }

        return $this->viewmodes[$identifier];
    }

    /**
     * @param string $identifier
     *
     * @return bool
     */
    public function exist($identifier)
    {
        return array_key_exists($identifier, $this->viewmodes);
    }
}
