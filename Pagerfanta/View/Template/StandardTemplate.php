<?php

namespace NM\Bundle\WebsiteBundle\Pagerfanta\View\Template;


use Pagerfanta\View\Template\TwitterBootstrapTemplate;

class StandardTemplate extends TwitterBootstrapTemplate
{
    protected static $overrideOptions = [
            'prev_message' => '&laquo;',
            'next_message' => '&raquo;',
            'css_container_class' => 'pagination-centered',
            'css_list_class' => 'pagination',
            'css_active_class' => 'current',
            'css_disabled_class' => 'unavailable',
            'css_dots_class' => 'unavailable',
            'css_prev_class' => 'arrow',
            'css_next_class' => 'arrow',
    ];

    public function __construct()
    {
        static::$defaultOptions = static::$overrideOptions + static::$defaultOptions;
        parent::__construct();
    }

    public function container()
    {
        return sprintf(
                '<div class="%s"><ul class="%s">%%pages%%</ul></div>',
                $this->option('css_container_class'),
                $this->option('css_list_class')
        );
    }

    public function nextDisabled()
    {
        $class = $this->nextDisabledClass();
        $text = $this->option('next_message');

        return $this->linkLi($class, "javascript:void(0);", $text);
    }

    public function previousDisabled()
    {
        $class = $this->previousDisabledClass();
        $text = $this->option('prev_message');

        return $this->linkLi($class, "javascript:void(0);", $text);
    }
    public function current($page)
    {
        $text = trim($page.' '.$this->option('active_suffix'));
        $class = $this->option('css_active_class');

        return $this->linkLi($class, "javascript:void(0);", $text);
    }

    private function previousDisabledClass()
    {
        return $this->option('css_prev_class').' '.$this->option('css_disabled_class');
    }

    private function nextDisabledClass()
    {
        return $this->option('css_next_class').' '.$this->option('css_disabled_class');
    }

    private function linkLi($class, $href, $text)
    {
        $liClass = $class ? sprintf(' class="%s"', $class) : '';

        return sprintf('<li%s><a href="%s">%s</a></li>', $liClass, $href, $text);
    }

    private function spanLi($class, $text)
    {
        $liClass = $class ? sprintf(' class="%s"', $class) : '';

        return sprintf('<li%s><span>%s</span></li>', $liClass, $text);
    }
}
