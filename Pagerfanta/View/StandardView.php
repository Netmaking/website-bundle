<?php

namespace NM\Bundle\WebsiteBundle\Pagerfanta\View;


use NM\Bundle\WebsiteBundle\Pagerfanta\View\Template\StandardTemplate;
use Pagerfanta\View\DefaultView;

class StandardView extends DefaultView
{
    public function getName()
    {
        return 'nm_standard';
    }

    protected function getDefaultProximity()
    {
        return 2;
    }

    protected function createDefaultTemplate()
    {
        return new StandardTemplate();
    }
}
