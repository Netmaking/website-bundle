<?php

namespace NM\Bundle\WebsiteBundle\Twig;

use eZ\Publish\API\Repository\ContentService;
use eZ\Publish\Core\Helper\TranslationHelper;
use eZ\Publish\Core\Repository\Values\Content\Content;

class ContentMappingExtension extends \Twig_Extension
{
    /**
     * @var TranslationHelper
     */
    private $translationHelper;
    /**
     * @var ContentService
     */
    private $contentService;

    function __construct($translationHelper, $contentService)
    {
        $this->translationHelper = $translationHelper;
        $this->contentService = $contentService;
    }

    public function getFunctions()
    {
        return array(
            new \Twig_SimpleFunction('nm_get_related_field', array($this, 'getRelatedField')),
            new \Twig_SimpleFunction('nm_get_related_fields', array($this, 'getRelatedFields')),
            new \Twig_SimpleFunction('nm_get_link_by_content_id', array($this, 'getLinkForContentId')),
            new \Twig_SimpleFunction('nm_get_tag_keyword', array($this, 'getTagKeyword'))
        );
    }

    public function getRelatedField($objectArray, $fieldName, $childrenArray = array())
    {
        if($objectArray instanceof Content) {
            $objectArray = array(
                'object' => $objectArray
            );
        }

        if(array_key_exists("children", $objectArray)) {
           $childrenArray = $objectArray["children"];
        }

        if (!array_key_exists($fieldName, $objectArray["object"]->fields)) {
            return null;
        }
        $field = $this->getTranslatedFieldValue($objectArray["object"], $fieldName);

        $destinationContentId = $field->destinationContentId;
        return $this->getChildElement($childrenArray, $destinationContentId);

    }

    public function getRelatedFields($object, $fieldName, $childrenArray = array())
    {
        $field = $this->getTranslatedFieldValue($object, $fieldName);
        $result = array();
        foreach ($field->destinationContentIds as $destinationContentId) {
            $result[] = $this->getChildElement($childrenArray, $destinationContentId);
        }
        return $result;
    }

    public function getLinkForContentId($contentId)
    {
        $content = $this->contentService->loadContent($contentId);
        return array(
            'name' => $this->translationHelper->getTranslatedContentName($content),
            'locationId' => $content->contentInfo->mainLocationId
        );
    }

    private function getChildElement($array, $id)
    {
        if (array_key_exists($id, $array)) {
            return $array[$id];
        }
        return null;
    }

    public function getTagKeyword($tag) {
        $languages = $this->translationHelper->getAvailableLanguages();

        foreach ($languages as $language) {
            if(array_key_exists($language, $tag->keywords))
            {
                return $tag->keywords[$language];
            }
        }
        return null;
    }

    public function getTranslatedFieldValue(Content $content, $fieldDefIdentifier, $forcedLanguage = null)
    {
        return $this->translationHelper->getTranslatedField($content, $fieldDefIdentifier, $forcedLanguage)->value;
    }

    /**
     * Returns the name of the extension.
     *
     * @return string The extension name
     */
    public function getName()
    {
        return 'nm_websitebundle_content_mapping_extension';
    }
}
