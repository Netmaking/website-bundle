<?php

namespace NM\Bundle\WebsiteBundle\Twig;

use Twig_Extension;
use Twig_SimpleFunction;

/**
 * Class StaticsExtension.
 *
 * @todo Replace this file
 */
class StaticsExtension extends Twig_Extension
{
    public function getFunctions()
    {
        return array(
            new Twig_SimpleFunction('getAlphabet', function ($language = null) {
                $alphabet = range('a', 'z');
                if (null !== $language) {
                    switch ($language) {
                        case 'no':
                        case 'no_NO':
                        case 'norwegian':
                            array_push($alphabet, 'æ', 'ø', 'å');
                        break;
                    }
                }

                return $alphabet;
            }),
            new Twig_SimpleFunction('getCounties', function ($language) {
                $counties = array();
                switch ($language) {
                    case 'no':
                    case 'no_NO':
                    case 'norwegian':
                        $counties[] = 'akershus';
                        $counties[] = 'aust-agder';
                        $counties[] = 'buskerud';
                        $counties[] = 'finnmark';
                        $counties[] = 'hedmark';
                        $counties[] = 'hordaland';
                        $counties[] = 'møre og romsdal';
                        $counties[] = 'nord-trøndelag';
                        $counties[] = 'nordland';
                        $counties[] = 'oppland';
                        $counties[] = 'oslo';
                        $counties[] = 'rogaland';
                        $counties[] = 'sogn og fjordane';
                        $counties[] = 'svalbard';
                        $counties[] = 'sør trøndelag';
                        $counties[] = 'telemark';
                        $counties[] = 'troms';
                        $counties[] = 'vest-agder';
                        $counties[] = 'vestfold';
                        $counties[] = 'østfold';
                    break;
                }

                return $counties;
            }),
        );
    }

    public function getName()
    {
        return 'nm_websitebundle_statics_extension';
    }
}
