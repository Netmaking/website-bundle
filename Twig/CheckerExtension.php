<?php

namespace NM\Bundle\WebsiteBundle\Twig;

class CheckerExtension extends \Twig_Extension
{
    public function getFilters()
    {
        return array(
            new \Twig_SimpleFilter('hasTotalCountAboveZero', array($this, 'hasTotalCountAboveZeroFilter')),
            new \Twig_SimpleFilter('transformLinksToHtml', array($this, 'transformLinksToHtmlFilter')),
        );
    }

    public function hasTotalCountAboveZeroFilter($parent = null)
    {
        if (isset($parent->totalCount) && $parent->totalCount > 0) {
            return true;
        }

        return false;
    }

    public function transformLinksToHtmlFilter($str = null)
    {
        $reg_exUrl = "/(http|https|ftp|ftps)\:\/\/[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}(\/\S*)?/";
        $urls = array();
        $urlsToReplace = array();
        if (preg_match_all($reg_exUrl, $str, $urls)) {
            $numOfMatches = count($urls[0]);
            for ($i = 0; $i < $numOfMatches; $i++) {
                $alreadyAdded = false;
                $numOfUrlsToReplace = count($urlsToReplace);
                for ($j = 0; $j < $numOfUrlsToReplace; $j++) {
                    if ($urlsToReplace[$j] == $urls[0][$i]) {
                        $alreadyAdded = true;
                    }
                }
                if (!$alreadyAdded) {
                    array_push($urlsToReplace, $urls[0][$i]);
                }
            }
            $numOfUrlsToReplace = count($urlsToReplace);
            for ($i = 0; $i < $numOfUrlsToReplace; $i++) {
                $str = str_replace($urlsToReplace[$i], '<a href="'.$urlsToReplace[$i].'">'.$urlsToReplace[$i].'</a> ', $str);
            }

            return $str;
        } else {
            return $str;
        }
    }

    public function getName()
    {
        return 'nm_websitebundle_checker_extension';
    }
}
