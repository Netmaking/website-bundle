<?php

namespace NM\Bundle\WebsiteBundle\Service;

use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use eZ\Publish\API\Repository\Repository as EzApiRepository;
use eZ\Publish\API\Repository\ContentService;

class NMContentService
{
    /**
     * @var Container
     */
    protected $container;

    /**
     * @var EzApiRepository
     */
    protected $repository;

    /**
     * @var ContentService
     */
    protected $ezContentService;

    /**
     * @param Container $container
     * @param EzApiRepository $repository
     */
    public function __construct(Container $container, EzApiRepository $repository)
    {
        $this->container = $container;
        $this->repository = $repository;
    }

    public function getEzContentService()
    {
        if (null === $this->ezContentService) {
            $this->ezContentService = $this->repository->getContentService();
        }

        return $this->ezContentService;
    }
}
