<?php


namespace NM\Bundle\WebsiteBundle\Service;

use eZ\Bundle\EzPublishCoreBundle\DependencyInjection\Configuration\ConfigResolver;
use eZ\Publish\API\Repository\Values\Content\Content;
use eZ\Publish\API\Repository\Values\Content\Query;
use eZ\Publish\API\Repository\Values\Content\Query\Criterion;
use eZ\Publish\API\Repository\Values\Content\Query\SortClause;
use Tedivm\StashBundle\Service\CacheService;

class NMMenuService
{
    /**
     * @var NMFieldService
     */
    private $nmFieldService;
    /**
     * @var NMLocationService
     */
    private $nmLocationService;
    /**
     * @var NMSearchService
     */
    private $nmSearchService;
    /**
     * @var NMContentService
     */
    private $nmContentService;
    /**
     * @var ConfigResolver
     */
    private $configResolver;
    /**
     * @var CacheService
     */
    private $pool;

    function __construct($nmFieldService, $nmLocationService, $nmSearchService, $nmContentService, $configResolver, $pool)
    {
        $this->nmFieldService = $nmFieldService;
        $this->nmLocationService = $nmLocationService;
        $this->nmSearchService = $nmSearchService;
        $this->nmContentService = $nmContentService;
        $this->configResolver = $configResolver;
        $this->pool = $pool;
    }

    public function getMenuItems($currentLocationId = null)
    {
        $currentPath = array();

        $rootLocationId = $this->configResolver->getParameter('content.tree_root.location_id');
        $rootLocation = $this->nmLocationService->getEzLocationService()->loadLocation($rootLocationId);

        /** @var \eZ\Publish\API\Repository\Values\Content\Search\SearchResult $mainmenuLocations */
        $mainmenuLocations = $this->nmSearchService->getEzSearchService()->findLocations(
                $this->nmSearchService->getChildLocationsQuery(
                        $rootLocationId,
                        null,
                        null,
                        new Criterion\LogicalAnd(
                                array(
                                        new Criterion\Field('show_in_menu', Criterion\Operator::EQ, true),
                                        new Criterion\Visibility(Criterion\Visibility::VISIBLE)
                                )
                        ),
                        array(new SortClause\Location\Priority(Query::SORT_ASC))
                )
        );

        if (count($mainmenuLocations->searchHits) == 0)
        {
            return array();
        }

        $mainmenu = array();

        /** @var \eZ\Publish\API\Repository\Values\Content\Search\SearchHit $hit */
        foreach ($mainmenuLocations->searchHits as $hit)
        {
            /** @var \eZ\Publish\Core\Repository\Values\Content\Location $location */
            $location = $hit->valueObject;
            /** @var Content $content */
            $content = $this->nmContentService->getEzContentService()->loadContent($location->contentId);
            if ($content->getFieldValue("show_in_menu"))
            {
                if ($this->nmFieldService->getEzFieldService()->isFieldEmpty($content, 'show_in_menu'))
                {
                    continue;
                }
            }

            $mainmenu[$location->id] = array(
                    'location' => $location,
                    'submenu' => array(),
            );

            if ($location->id == $currentLocationId)
            {
                $currentPath = explode("/", $location->pathString);
            }

            if ($content->getFieldValue("expand_in_menu"))
            {
                if ($this->nmFieldService->getEzFieldService()->isFieldEmpty($content, 'expand_in_menu'))
                {
                    $mainmenu[$location->id]['submenu'] = false;
                }
            }
        }

        $submenuLocations = $this->nmSearchService->getEzSearchService()->findLocations(
                $this->nmSearchService->getChildLocationsQuery(
                        null,
                        null,
                        null,
                        new Criterion\LogicalAnd(
                                array(
                                        new Criterion\Field('show_in_menu', Criterion\Operator::EQ, true),
                                        new Criterion\Visibility(Criterion\Visibility::VISIBLE)
                                )
                        ),
                        array(new SortClause\Location\Priority(Query::SORT_ASC))
                )
        );

        /** @var \eZ\Publish\API\Repository\Values\Content\Search\SearchHit $hit */
        foreach ($submenuLocations->searchHits as $hit)
        {

            /** @var \eZ\Publish\Core\Repository\Values\Content\Location $location */
            $location = $hit->valueObject;
            if (!array_key_exists($location->parentLocationId, $mainmenu))
            {
                continue;
            }

            if (false === $mainmenu[$location->parentLocationId]['submenu'])
            {
                continue;
            }

            $content = $this->nmContentService->getEzContentService()->loadContent($location->contentId);
            if ($content->getFieldValue("show_in_menu"))
            {
                if ($this->nmFieldService->getEzFieldService()->isFieldEmpty($content, 'show_in_menu'))
                {
                    continue;
                }
            }

            if ($location->id == $currentLocationId)
            {
                $currentPath = explode("/", $location->pathString);
            }

            $mainmenu[$location->parentLocationId]['submenu'][] = $location;


        }

        return array(
                'mainmenu' => $mainmenu,
                'currentLocationId' => $currentLocationId,
                'currentPath' => $currentPath
        );

    }
}
