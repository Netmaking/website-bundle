<?php

namespace NM\Bundle\WebsiteBundle\Service;

use eZ\Publish\API\Repository\Values\Content\Query\Criterion;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use eZ\Publish\API\Repository\Values\Content;

/**
 * Class NMSearchService.
 */
class NMSearchService
{
    /**
     * @var Container
     */
    protected $container;

    /**
     * @var \eZ\Publish\API\Repository\Repository
     */
    protected $repository;

    /**
     * @var \eZ\Publish\API\Repository\SearchService
     */
    protected $ezSearchService;

    /**
     * @param Container $container
     */
    public function __construct(Container $container)
    {
        $this->container = $container;
        $this->repository = $container->get('ezpublish.api.repository');
    }

    /**
     * @return \eZ\Publish\API\Repository\SearchService
     */
    public function getEzSearchService()
    {
        if (null === $this->ezSearchService) {
            $this->ezSearchService = $this->repository->getSearchService();
        }

        return $this->ezSearchService;
    }

    /**
     * @param null|int          $locationId
     * @param null|string|array $identifiers
     * @param null|int          $depth
     * @param bool              $filterOnPermissions
     *
     * @return Content\Search\SearchResult
     */
    public function getChildLocations($locationId = null, $identifiers = null, $depth = null, $filterOnPermissions = true)
    {
        return $this->getEzSearchService()->findLocations(
            $this->getChildLocationsQuery($locationId, $identifiers, $depth),
            $filterOnPermissions
        );
    }

    /**
     * Method to use in (e.g.) LocationSearchAdapter for Pagerfanta.
     *
     * @param null|int                  $locationId
     * @param null|string|array         $identifiers
     * @param null|int                  $depth
     * @param null|Criterion $fieldFilters
     * @param null|array                $sortClause
     *
     * @return Content\LocationQuery
     */
    public function getChildLocationsQuery(
        $locationId = null, $identifiers = null, $depth = null, $fieldFilters = null, $sortClause = null
    ) {
        $criterions = array();
        if (null !== $locationId) {
            $criterions[] = new Criterion\ParentLocationId($locationId);
        }

        if (null !== $identifiers) {
            $criterions[] = $this->getIdentifierCriterion($identifiers);
        }

        if (null !== $depth) {
            $criterions[] = new Criterion\Location\Depth(Criterion\Operator::EQ, $depth);
        }

        if (null !== $fieldFilters) {
            $criterions[] = $fieldFilters;
        }

        $query = new Content\LocationQuery();

        if (null !== $sortClause) {
            $query->sortClauses = $sortClause;
        }

        $query->criterion = new Criterion\LogicalAnd($criterions);

        return $query;
    }

    /**
     * @param string|array $identifiers
     *
     * @return Criterion\LogicalOr
     */
    protected function getIdentifierCriterion($identifiers)
    {
        if (!is_array($identifiers)) {
            $identifiers = [$identifiers];
        }
        $contentTypeIdentifiers = array();
        foreach ($identifiers as $identifier) {
            $contentTypeIdentifiers[] = new Criterion\ContentTypeIdentifier($identifier);
        }

        return new Criterion\LogicalOr($contentTypeIdentifiers);
    }

    public function createCriterion(
        $locationId = null,
        $identifiers = null,
        $depth = null,
        $sortClause = null,
        $fieldFilters = null
    ) {
    }
}
