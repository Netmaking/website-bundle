<?php

namespace NM\Bundle\WebsiteBundle\Service;

use eZFunctionHandler;
use eZContentObjectTreeNode;
use Tedivm\StashBundle\Service\CacheService;

class NMLegacyService
{
    /** @var callable $legacyKernel */
    protected $legacyKernel;

    /** @var CacheService $cachePool */
    protected $cachePool;

    /** @var int $defaultTtl */
    protected $defaultTtl;

    /**
     * @param \Closure $kernel
     * @param CacheService $cacheService
     */
    public function __construct(\Closure $kernel, CacheService $cacheService, $defaultTtl)
    {
        $this->legacyKernel = $kernel;
        $this->cachePool = $cacheService;
        $this->defaultTtl = $defaultTtl;
    }

    /**
     * @return \eZ\Publish\Core\MVC\Legacy\Kernel
     */
    public function getLegacyKernel()
    {
        $kernelClosure = $this->legacyKernel;

        return $kernelClosure();
    }

    /**
     * @param int $id
     *
     * @return eZContentObjectTreeNode|false
     */
    public function getNodeByLocationId($id)
    {
        $cacheKeys = ['legacy', 'location', $id];
        $cache = $this->cachePool->getItem($cacheKeys);

        if ($cache->isMiss()) {
            $node = $this->getLegacyKernel()->runCallback(
                    function () use ($id) {
                        return eZFunctionHandler::execute(
                                'content', 'node', array('node_id' => $id)
                        );
                    }
            );
            $cache->set($node, $this->defaultTtl);
        }else{
            $node = $cache->get();
        }

        if (!$node instanceof eZContentObjectTreeNode) {
            return false;
        }

        return $node;
    }

    /**
     * @param eZContentObjectTreeNode $node
     * @param string                  $keyword
     *
     * @return bool
     */
    public function nodeHasKeyword($content, $keyword)
    {
        $tags = $content->getFieldValue("tags")->tags;
        foreach ($tags as $tag) {
            if (strtoupper($tag->getKeyword()) == strtoupper($keyword)) {
                return true;
            }
        }

        return false;
    }
}
