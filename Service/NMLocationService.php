<?php

namespace NM\Bundle\WebsiteBundle\Service;

use eZ\Publish\API\Repository\Values\Content\Query\Criterion;
use eZ\Publish\API\Repository\Values\Content\LocationQuery;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use eZ\Publish\API\Repository\LocationService;
use eZ\Publish\API\Repository\Repository as EzApiRepository;

class NMLocationService
{
    /** @var Container */
    protected $container;

    /** @var EzApiRepository */
    protected $repository;

    /** @var LocationService */
    protected $ezLocationService;

    /** @var NMSearchService $nmSearchService */
    protected $nmSearchService;

    /** @var NMContentService $nmContentService */
    protected $nmContentService;

    /** @var SortLocationService $sortLocationService */
    protected $sortLocationService;

    /**
     * @param Container $container
     * @param NMSearchService $nmSeachService
     * @param NMContentService $nmContentService
     * @param SortLocationService $sortLocationService
     */
    public function __construct(
        Container $container,
        NMSearchService $nmSeachService,
        NMContentService $nmContentService,
        SortLocationService $sortLocationService
    ) {
        $this->container = $container;
        $this->repository = $container->get('ezpublish.api.repository');
        $this->nmSearchService = $nmSeachService;
        $this->nmContentService = $nmContentService;
        $this->sortLocationService = $sortLocationService;
    }

    /**
     * @return \eZ\Publish\API\Repository\LocationService
     */
    public function getEzLocationService()
    {
        if (null === $this->ezLocationService) {
            $this->ezLocationService = $this->repository->getLocationService();
        }

        return $this->ezLocationService;
    }

    /**
     * @param int   $locationId
     * @param array $identifiers
     *
     * @return \eZ\Publish\API\Repository\Values\Content\Search\SearchResult
     */
    public function getLocationsByIdentifier($locationId, array $identifiers)
    {
        $identifierCriterion = false;
        if (count($identifiers) > 0) {
            $identifierCriterion = new Criterion\LogicalOr(
                array(
                    new Criterion\ContentTypeIdentifier($identifiers),
                    new Criterion\ContentTypeId($identifiers),
                )
            );
        }

        $criteria = array();
        $criteria[] = new Criterion\ParentLocationId($locationId);

        if (false !== $identifierCriterion) {
            $criteria[] = $identifierCriterion;
        }

        $query = new LocationQuery();
        $query->criterion = new Criterion\LogicalAnd(
            $criteria
        );

        $location = $this->getEzLocationService()->loadLocation($locationId);
        $query->sortClauses = array(
            $this->sortLocationService->getSortClauseFromLocation(
                    $location
            )
        );

        return $this->nmSearchService->getEzSearchService()->findLocations($query);
    }

    /**
     * Returns a list of locations from a locations object relation field.
     *
     * @param int    $locationId
     * @param string $fieldIdentifier
     * @param int    $offset
     * @param int    $limit
     *
     * @return \eZ\Publish\API\Repository\Values\Content\LocationList
     */
    public function getChildrenOfRelationField($locationId, $fieldIdentifier, $offset = 0, $limit = -1)
    {
        $location = $this->getEzLocationService()->loadLocation($locationId);
        $content = $this->nmContentService->getEzContentService()->loadContentByContentInfo($location->contentInfo);

        if (!$content->__isset($fieldIdentifier)) {
            /* @todo implement handling if relation not set */
        }

        /** @var \eZ\Publish\Core\FieldType\Relation\Value $relationValue */
        $relationValue = $content->getFieldValue($fieldIdentifier);

        $contentInfo = $this->nmContentService->getEzContentService()->loadContentInfo($relationValue->destinationContentId);
        //echo $relationValue->destinationContentId; die;
        $relationFieldLocation = $this->getEzLocationService()->loadLocation($contentInfo->mainLocationId);

        return $this->getEzLocationService()->loadLocationChildren($relationFieldLocation);
    }

    /**
     * Main action for viewing content through a location in the repository.
     * Response will be cached with HttpCache validation model (Etag).
     *
     * @param int    $locationId
     * @param string $viewType
     * @param bool   $layout
     * @param array  $params
     *
     * @throws \Symfony\Component\Security\Core\Exception\AccessDeniedException
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     * @throws \Exception
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function generateResponse($locationId, $viewType, $layout = false, $params = array())
    {
        /** @var \eZ\Publish\Core\MVC\Symfony\Controller\Content\ViewController $viewController */
        $viewController = $this->container->get('ez_content');

        /** @var \Symfony\Component\HttpFoundation\Response $response */
        $response = $viewController->viewLocation(
            $locationId,
            $viewType,
            $layout,
            array(
                'locationId' => $locationId,
            ) + $params
        );

        return $response;
    }
}
