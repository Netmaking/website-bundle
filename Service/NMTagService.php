<?php

namespace NM\Bundle\WebsiteBundle\Service;

use Symfony\Component\Translation\TranslatorInterface as Translator;
use Netgen\TagsBundle\Core\Repository\TagsService;

/**
 * Class NMTagService.
 */
class NMTagService
{
    /** @var Translator $translator */
    protected $translator;

    /** @var NMFieldService $nmFieldService */
    protected $nmFieldService;

    /** @var TagsService $ngTagsService */
    protected $ngTagsService;

    /**
     * @param Translator     $translator
     * @param NMFieldService $nmFieldService
     * @param TagsService    $ngTagService
     */
    public function __construct(
        Translator $translator,
        NMFieldService $nmFieldService,
        TagsService $ngTagService
    ) {
        $this->translator = $translator;
        $this->nmFieldService = $nmFieldService;
        $this->ngTagsService = $ngTagService;
    }

    /**
     * @return TagsService
     */
    public function getNetgenTagService()
    {
        return $this->ngTagsService;
    }

    /**
     * Returns an array with keyword as key and count as value.
     *
     * @param array $result
     *
     * @return array
     *
     * @example [
     *      'All'         => 6,
     *      'Some tag'    => 2,
     *      'Another tag' => 5
     *  ]
     */
    public function getTagsInSubtree(array $result)
    {
        $allString = $this->translator->trans('All');

        $tags = [
            $allString => 0,
        ];

        foreach ($result as $location) {
            /** @var \Netgen\TagsBundle\Core\FieldType\Tags\Value $contentTags */
            if (false !== $contentTags = $this->nmFieldService->getFieldValue($location->id, 'tags')) {
                $tags[$allString]++;
                foreach ($contentTags->tags as $tag) {
                    if (!array_key_exists($tag->keyword, $tags)) {
                        $tags[$tag->keyword] = 1;
                    } else {
                        $tags[$tag->keyword]++;
                    }
                }
            }
        }

        return $tags;
    }
}
