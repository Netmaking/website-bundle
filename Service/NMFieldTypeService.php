<?php

namespace NM\Bundle\WebsiteBundle\Service;

use Symfony\Component\DependencyInjection\ContainerInterface as Container;

class NMFieldTypeService
{
    /**
     * @var Container
     */
    protected $container;

    /**
     * @var \eZ\Publish\API\Repository\Repository
     */
    protected $repository;

    /**
     * @var \eZ\Publish\API\Repository\FieldTypeService
     */
    protected $ezFieldTypeService;

    /**
     * @param Container $container
     */
    public function __construct(Container $container)
    {
        $this->container = $container;
        $this->repository = $container->get('ezpublish.api.repository');
    }

    /**
     * @return \eZ\Publish\API\Repository\FieldTypeService
     */
    public function getEzFieldTypeService()
    {
        if (null === $this->ezFieldTypeService) {
            $this->ezFieldTypeService = $this->repository->getFieldTypeService();
        }

        return $this->ezFieldTypeService;
    }
}
