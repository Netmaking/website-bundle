<?php

namespace NM\Bundle\WebsiteBundle\Service;

use Symfony\Component\DependencyInjection\ContainerInterface as Container;

class NMContentTypeService
{
    /**
     * @var Container
     */
    protected $container;

    /**
     * @var \eZ\Publish\API\Repository\Repository
     */
    protected $repository;

    /**
     * @var \eZ\Publish\API\Repository\ContentTypeService
     */
    protected $ezContentTypeService;

    /**
     * @param Container $container
     */
    public function __construct(Container $container)
    {
        $this->container = $container;
        $this->repository = $container->get('ezpublish.api.repository');
    }

    public function getEzContentTypeService()
    {
        if (null === $this->ezContentTypeService) {
            $this->ezContentTypeService = $this->repository->getContentTypeService();
        }

        return $this->ezContentTypeService;
    }
}
