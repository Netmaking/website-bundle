<?php

/**
 * Extending the LocationService class to be able to use protected function getSortClauseBySortField()
 *
 * The purpose is to sort searches by sort settings set on the location (in admin interface)
 */

namespace NM\Bundle\WebsiteBundle\Service;

use eZ\Publish\Core\Repository\LocationService;
use eZ\Publish\API\Repository\Values\Content\Location;

/**
 * Class SortLocationService
 * @package NM\Bundle\WebsiteBundle\Service
 */
class SortLocationService extends LocationService
{
    public function __construct()
    {
    }

    /**
     * @param Location $location
     * @return \eZ\Publish\API\Repository\Values\Content\Query\SortClause
     */
    public function getSortClauseFromLocation( Location $location )
    {
        return $this->getSortClauseBySortField( $location->sortField,
                $location->sortOrder );
    }
}
