<?php

namespace NM\Bundle\WebsiteBundle\Service;

use eZ\Publish\API\Repository\Values\Content\Query\Criterion;
use eZ\Publish\API\Repository\Values\Content\Location;
use eZ\Publish\Core\Helper\FieldHelper;

class NMFieldService
{
    /**
     * @var FieldHelper
     */
    protected $ezFieldService;

    /**
     * @var NMLocationService
     */
    protected $nmLocationService;

    /**
     * @var NMContentService
     */
    protected $nmContentService;

    /**
     * @var NMSearchService
     */
    protected $nmSearchService;

    /**
     * @param FieldHelper       $ezFieldService
     * @param NMLocationService $nmLocationService
     * @param NMContentService  $nmContentService
     * @param NMSearchService   $nmSearchService
     */
    public function __construct(
        FieldHelper $ezFieldService,
        NMLocationService $nmLocationService,
        NMContentService $nmContentService,
        NMSearchService $nmSearchService
    ) {
        $this->ezFieldService = $ezFieldService;
        $this->nmLocationService = $nmLocationService;
        $this->nmContentService = $nmContentService;
        $this->nmSearchService = $nmSearchService;
    }

    /**
     * @return \eZ\Publish\Core\Helper\FieldHelper
     */
    public function getEzFieldService()
    {
        return $this->ezFieldService;
    }

    /**
     * @param int    $locationId
     * @param string $typeIdentifier
     *
     * @return Location|false
     */
    public function getFieldRelation($locationId, $typeIdentifier)
    {
        $criterions = array(
            new Criterion\ParentLocationId($locationId),
            new Criterion\Visibility(Criterion\Visibility::VISIBLE),
            new Criterion\ContentTypeIdentifier($typeIdentifier),
        );

        $result = $this->nmSearchService->getEzSearchService()->findSingle(new Criterion\LogicalAnd($criterions));

        return $this->nmLocationService->getEzLocationService()->loadLocation($result->contentInfo->mainLocationId);
    }

    /**
     * @param int    $locationId
     * @param string $fieldIdentifier
     *
     * @return mixed|bool a primitive type or a field type Value object depending on the field type.
     */
    public function getFieldValue($locationId, $fieldIdentifier)
    {
        $location = $this->nmLocationService->getEzLocationService()->loadLocation($locationId);
        $content = $this->nmContentService->getEzContentService()->loadContent($location->contentId);

        $value = $content->getFieldValue($fieldIdentifier);
        if ('' == trim($value)) {
            return false;
        }

        return $value;
    }
}
