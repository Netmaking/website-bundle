<?php

namespace NM\Bundle\WebsiteBundle\ViewModes;

use eZ\Publish\API\Repository\Values\Content\Query\Criterion;
use eZ\Publish\API\Repository\Values\Content\Query\Criterion\Operator;
use Netgen\TagsBundle\API\Repository\Values\Content\Query\Criterion\TagKeyword;
use NM\Bundle\WebsiteBundle\Viewmode\ViewmodeInterface;
use Symfony\Component\HttpFoundation\Request;
use NM\Bundle\WebsiteBundle\Viewmode\Viewmode;
use eZ\Publish\API\Repository\Values\Content\Query;
use eZ\Publish\API\Repository\Values\Content\Query\SortClause;

class Article extends Viewmode implements ViewmodeInterface
{
    private $currentTag;

    /**
     * @inheritdoc
     */
    public function getFieldCriterions(Request $request)
    {
        $criterions = $this->getFilterCriterions($request);
        $criterions[] = new Criterion\Field('publish_date', Criterion\Operator::LT, time());
        $criterions[] = new Criterion\Visibility(Criterion\Visibility::VISIBLE);

        if($this->currentTag !== null) {
            //$criterions[] = new TagKeyword("tags", Operator::EQ, $this->currentTag);
        }

        return count($criterions) > 0 ? new Criterion\LogicalAnd($criterions) : null;
    }

    public function setCurrentTag($tag)
    {
        $this->currentTag = $tag;
    }

    /**
     * @inheritdoc
     */
    public function getSortClause()
    {
        /**
         * Hardcoding translation is mandatory until the following ticket is solved
         *
         * @see https://jira.ez.no/browse/EZP-23393
         *
         * @todo Remove 'nor-NO'
         *
         */

        return false;
    }

    /**
     * @inheritdoc
     */
    public function getContentTypeIdentifiers()
    {
        return ['endowment_article'];
    }

    /**
     * @inheritdoc
     */
    public function getTemplate()
    {
        return 'pages-artikkelliste.twig';
    }

    /**
     * @inheritdoc
     */
    public function getIdentifier()
    {
        return 'line_article';
    }

    /**
     * @inheritdoc
     */
    public function usePagination()
    {
        return true;
    }
}
