<?php

namespace NM\Bundle\WebsiteBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files.
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html#cookbook-bundles-extension-config-class}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritdoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('nm_website');

        $rootNode
            ->children()
                ->arrayNode('ttl')
                    ->children()
                        ->integerNode('relations')->defaultValue(60)->end()
                        ->integerNode('footer_twitter')->defaultValue(60)->end()
                        ->integerNode('breadcrumb')->defaultValue(60)->end()
                    ->end()
                ->end() // ttl
            ->end();
        ;

        return $treeBuilder;
    }
}
