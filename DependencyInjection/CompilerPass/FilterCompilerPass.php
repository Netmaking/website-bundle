<?php

namespace NM\Bundle\WebsiteBundle\DependencyInjection\CompilerPass;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\Reference;

/**
 * Class ViewmodesCompilerPass.
 */
class FilterCompilerPass implements CompilerPassInterface
{
    /**
     * @param ContainerBuilder $container
     */
    public function process(ContainerBuilder $container)
    {
        if (!$container->hasDefinition('nm.criterion.filter.chain')) {
            return;
        }

        $definition = $container->getDefinition(
            'nm.criterion.filter.chain'
        );

        $taggedServices = $container->findTaggedServiceIds(
            'nm.criterion.filter'
        );

        foreach ($taggedServices as $id => $tags) {
            $definition->addMethodCall(
                'add',
                array(new Reference($id))
            );
        }
    }
}
