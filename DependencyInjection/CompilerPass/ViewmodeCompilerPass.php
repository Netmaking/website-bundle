<?php

namespace NM\Bundle\WebsiteBundle\DependencyInjection\CompilerPass;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\Reference;

/**
 * Class ViewmodesCompilerPass.
 */
class ViewmodeCompilerPass implements CompilerPassInterface
{
    /**
     * @param ContainerBuilder $container
     */
    public function process(ContainerBuilder $container)
    {
        if (!$container->hasDefinition('nm.folder.viewmode.chain')) {
            return;
        }

        $definition = $container->getDefinition(
            'nm.folder.viewmode.chain'
        );

        $taggedServices = $container->findTaggedServiceIds(
            'nm.folder.viewmode'
        );

        foreach ($taggedServices as $id => $tags) {
            $definition->addMethodCall(
                'add',
                array(new Reference($id))
            );
        }
    }
}
