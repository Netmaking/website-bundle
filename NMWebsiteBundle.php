<?php

namespace NM\Bundle\WebsiteBundle;

use NM\Bundle\WebsiteBundle\DependencyInjection\CompilerPass\FilterCompilerPass;
use NM\Bundle\WebsiteBundle\DependencyInjection\CompilerPass\ViewmodeCompilerPass;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class NMWebsiteBundle extends Bundle
{
    public function build(ContainerBuilder $container)
    {
        parent::build($container);

        $container->addCompilerPass(new ViewmodeCompilerPass());
        $container->addCompilerPass(new FilterCompilerPass());
    }
}
