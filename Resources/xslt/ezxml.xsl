<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet
        version="1.0"
        xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
        xmlns:xhtml="http://ez.no/namespaces/ezpublish3/xhtml/"
        xmlns:custom="http://ez.no/namespaces/ezpublish3/custom/"
        xmlns:image="http://ez.no/namespaces/ezpublish3/image/"
        exclude-result-prefixes="xhtml custom image">

    <xsl:template match="custom[@name='factbox']">
        <xsl:if test="@custom:align ='right'">
            <div class="panel radius callout part13full right">
                <xsl:copy-of select="@*"/>
                <xsl:if test="@custom:title !=''">
                    <h3>
                        <xsl:value-of select="@custom:title"/>
                    </h3>
                </xsl:if>
                <xsl:apply-templates/>
            </div>
        </xsl:if>
        <xsl:if test="@custom:align ='left'">
            <div class="panel radius callout part13full left">
                <xsl:copy-of select="@*"/>
                <xsl:if test="@custom:title !=''">
                    <h3>
                        <xsl:value-of select="@custom:title"/>
                    </h3>
                </xsl:if>
                <xsl:apply-templates/>
            </div>
        </xsl:if>
        <xsl:if test="@custom:align !='right' and @custom:align !='left'">
            <div class="panel radius callout">
                <xsl:copy-of select="@*"/>
                <xsl:if test="@custom:title !=''">
                    <h3>
                        <xsl:value-of select="@custom:title"/>
                    </h3>
                </xsl:if>
                <xsl:apply-templates/>
            </div>
        </xsl:if>
    </xsl:template>
</xsl:stylesheet>
