<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet
        version="1.0"
        xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
        xmlns:xhtml="http://ez.no/namespaces/ezpublish3/xhtml/"
        xmlns:custom="http://ez.no/namespaces/ezpublish3/custom/"
        xmlns:image="http://ez.no/namespaces/ezpublish3/image/"
        exclude-result-prefixes="xhtml custom image">

    <xsl:template match="custom[@name='quote']">
        <blockquote>
            <xsl:apply-templates/>
            <xsl:if test="@custom:author !=''">
                <p>
                    <cite>
                        <xsl:value-of select="@custom:author"/>
                    </cite>
                </p>
            </xsl:if>
        </blockquote>

    </xsl:template>
</xsl:stylesheet>
