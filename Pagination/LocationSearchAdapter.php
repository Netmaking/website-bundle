<?php

namespace NM\Bundle\WebsiteBundle\Pagination;


use NM\Raeder\IntranetBundle\Pagerfanta\LocationSearchHitAdapter;

class LocationSearchAdapter extends LocationSearchHitAdapter
{
    /**
     * Returns a slice of the results as Location objects.
     *
     * @param int $offset The offset.
     * @param int $length The length.
     *
     * @return \eZ\Publish\API\Repository\Values\Content\Location[]
     */
    public function getSlice($offset, $length)
    {
        $list = array();
        foreach (parent::getSlice($offset, $length) as $hit) {
            $list[] = $hit->valueObject;
        }
        //echo count($list); die;
        return $list;
    }
}
